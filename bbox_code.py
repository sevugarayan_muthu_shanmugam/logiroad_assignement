import cv2 as cv    #openCV library - computer vision library
import numpy as np  #Numpy library- array processing package
import os           # used to access operating system functionalities
 
class BBOX():
    def __init__(self,input_dic={}): #It takes dictionary as input argument
        self.input_dic=input_dic     #Attribute of class
        self.Creat_Folder()          #Calling Function 
        self.det_BB()                #Calling Function
        
    
    #This method helps to creat the folder in current working directory to save final results. 
    def Creat_Folder(self):
        if not os.path.exists("Results"):     #Checks whether the folder with name "Results" exists or not
            os.makedirs("Results")            #It created folder "Results" as if does not exists in current wworking directory 
        fileDir = os.path.dirname(os.path.realpath('__file__')) #Indentifies the path
        self.filename = os.path.join(fileDir, 'Results')        #merging the path of current working directory with newly created folder
    
    
    #This method helps to creat rectangle on images
    def det_BB(self):
        for key in self.input_dic:  #self.input_dic has stores the image name and respective bounding box
            img = cv.imread(key, cv.IMREAD_COLOR) #Read the image in color format 
            for aa in self.input_dic[key]:  
                cv.rectangle(img, (aa[0],aa[1]),(aa[2],aa[3]),(0,0,255),5) #this function helps to form the rectangle with given coordinates
                cv.imwrite(self.filename + "/{}".format(key), img)  #saves the file in separate folder
        
if __name__ == "__main__" :
    #Created Object for class BBOX and it takes one input argument in the form of dictionary.
    #Image name is a key of dic and bounding box is value of dic
    obj=BBOX(input_dic={"000010.png":[(373,205,401,253),(100,259,170,301)], "000005.png":[(157,62,403,255)], "000009.png":[(12,34,475,332)],"000011.png":[(121,60,474,272),(51,116,248,264)],"000014.png":[(241,296,310,331)]})
        
